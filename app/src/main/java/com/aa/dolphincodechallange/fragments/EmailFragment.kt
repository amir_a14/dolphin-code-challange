package com.aa.dolphincodechallange.fragments

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.aa.dolphincodechallange.R
import com.aa.dolphincodechallange.databinding.FragmentEmailBinding
import com.aa.dolphincodechallange.viewmodels.EmailFragmentViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class EmailFragment : Fragment(R.layout.fragment_email) {
    //provide viewModel with kotlin ktx extensions
    private val viewModel: EmailFragmentViewModel by viewModels()


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //Provide DataBinding
        val binding = FragmentEmailBinding.bind(view)
        binding.viewModel = viewModel
        //This line is need for live data to change views with data binding
        binding.lifecycleOwner = viewLifecycleOwner

        viewModel.isExistEmail.observe(viewLifecycleOwner, {
            findNavController().navigate(if (it) R.id.action_emailFragment_to_passwordFragment
            else R.id.action_emailFragment_to_signUpFragment)
        })
    }
}