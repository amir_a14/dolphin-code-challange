package com.aa.dolphincodechallange.fragments

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.aa.dolphincodechallange.R
import com.aa.dolphincodechallange.activities.MainActivity
import com.aa.dolphincodechallange.databinding.FragmentPasswordBinding
import com.aa.dolphincodechallange.viewmodels.PasswordFragmentViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PasswordFragment : Fragment(R.layout.fragment_password) {
    //provide viewModel with kotlin ktx extensions
    private val viewModel: PasswordFragmentViewModel by viewModels()


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //Provide DataBinding
        val binding = FragmentPasswordBinding.bind(view)
        binding.viewModel = viewModel
        //This line is need for live data to change views with data binding
        binding.lifecycleOwner = viewLifecycleOwner

        viewModel.isPasswordCorrect.observe(viewLifecycleOwner, {
            if (it) {
                Toast.makeText(context, "Welcome to home!", Toast.LENGTH_LONG).show()
                startActivity(Intent(activity, MainActivity::class.java))
                activity?.finish()
            }
        })
    }
}