package com.aa.dolphincodechallange.viewmodels

import android.app.Application
import androidx.hilt.lifecycle.ViewModelInject


class MainActivityViewModel @ViewModelInject constructor(application: Application) :
    BaseViewModel(application) {
}