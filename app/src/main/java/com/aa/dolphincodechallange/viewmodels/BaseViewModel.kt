package com.aa.dolphincodechallange.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.aa.dolphincodechallange.utils.MyApplication

open class BaseViewModel(application: Application) : AndroidViewModel(application) {
    val application: MyApplication by lazy { getApplication<MyApplication>() }
    val isLoading: MutableLiveData<Boolean> by lazy { MutableLiveData(false) }
    val message: MutableLiveData<String> by lazy { MutableLiveData() }
}