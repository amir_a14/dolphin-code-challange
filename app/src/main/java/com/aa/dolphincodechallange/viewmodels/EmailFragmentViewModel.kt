package com.aa.dolphincodechallange.viewmodels

import android.app.Application
import android.os.Handler
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import com.aa.dolphincodechallange.R


class EmailFragmentViewModel @ViewModelInject constructor(application: Application) :
    BaseViewModel(application) {
    val isExistEmail: MutableLiveData<Boolean> by lazy { MutableLiveData() }
    fun sendEmail(email: String?) {
        if (checkInputs(email)) {
            isLoading.value = true
            Handler().postDelayed({
                isLoading.value = false
                isExistEmail.value = email == "a.abbasj@yahoo.com"
            }, 1500)
        }
    }

    private fun checkInputs(email: String?): Boolean {
        if (email.isNullOrEmpty()) {
            message.value = application.getString(R.string.email_error)
            return false
        }
        return true
    }
}