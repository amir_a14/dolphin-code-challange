package com.aa.dolphincodechallange.viewmodels

import android.app.Application
import android.os.Handler
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import com.aa.dolphincodechallange.R


class SignUpFragmentViewModel @ViewModelInject constructor(application: Application) :
    BaseViewModel(application) {
    val isSignUpSuccess: MutableLiveData<Boolean> by lazy { MutableLiveData() }
    fun doSignUp(name: String, pass: String?, isAgree: Boolean) {
        if (checkInputs(name, pass, isAgree)) {
            isLoading.value = true
            Handler().postDelayed({
                isLoading.value = false
                isSignUpSuccess.value = true
            }, 1500)
        }
    }

    private fun checkInputs(name: String?, pass: String?, isAgree: Boolean): Boolean {
        if (name.isNullOrEmpty()) {
            message.value = application.getString(R.string.name_error)
            return false
        }
        if (pass.isNullOrEmpty()) {
            message.value = application.getString(R.string.password_error)
            return false
        }
        if (!isAgree) {
            message.value = application.getString(R.string.you_must_agree);
            return false
        }
        return true
    }
}