package com.aa.dolphincodechallange.viewmodels

import android.app.Application
import android.os.Handler
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import com.aa.dolphincodechallange.R


class PasswordFragmentViewModel @ViewModelInject constructor(application: Application) :
    BaseViewModel(application) {
    val isPasswordCorrect: MutableLiveData<Boolean> by lazy { MutableLiveData() }
    fun doLogin(pass: String?) {
        if (checkInputs(pass)) {
            isLoading.value = true
            Handler().postDelayed({
                isLoading.value = false
                isPasswordCorrect.value = pass == "1234"
                if (pass != "1234")
                    message.value = application.getString(R.string.password_incorrect)
            }, 1500)
        }
    }

    private fun checkInputs(pass: String?): Boolean {
        if (pass.isNullOrEmpty()) {
            message.value = application.getString(R.string.password_error);
            return false
        }
        return true
    }
}